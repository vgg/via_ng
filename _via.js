
////////////////////////////////////////////////////////////////////////////////
//
// @file        _via.js
// @description VGG Image Annotator (VIA) application entrypoint
// @author      Abhishek Dutta <adutta@robots.ox.ac.uk>
// @date        17 June 2017
//
////////////////////////////////////////////////////////////////////////////////

function _via() {
  this.m = new _via_model();  // model
  this.v = new _via_view();   // view
  this.c = new _via_ctrl();   // controller

  this.init = function(view_panel, message_panel) {
    console.log("Initializing _VggImageAnnotator ...");
    this.m.init( this.c );
    this.v.init( this.c, view_panel, message_panel );
    this.c.init( this.m, this.v );
  }
}

_via.prototype.register_ui_action_handler = function(html_element) {
  var p = document.getElementById(html_element);
  if(p) {
    p.addEventListener("click", this.v, false);
  }
}
