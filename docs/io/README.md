# Input/Output Features of VIA

We support export of region shape and attributes in several formats.

## Export
### Single File Format
 * Results in a single CSV file containing all the data
 * a bit complex to analyze using scripts
```
#filename,file_size,file_attributes,region_count,region_id,region_shape_attributes,region_attributes
...
```

### Multiple Files Format
 * Results in the data exported in 3 different files
 * much simpler to analyze using scripts


Filename: file_index.csv
```
#file_id,filename,filesize
```

Filename: file_index.csv
```
#file_id,filename,filesize
```

Filename: regions_rect.csv
```
filename,filesize,region_id,x,y,width,height
```

Filename: regions_circle.csv
```
filename,filesize,region_id,cx,cy,r
```
