# VIA Source Code Documentation

VIA source code is organized using the [Model View Controller (MVC)](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller) 
design pattern for softwares as shown below:
![](docs/code_documentation/via_mvc_design.png)


Abhishek Dutta  
17 July 2017
