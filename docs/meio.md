# Metadata Input/Output (MEIO)
This document describes the metadata input/output environment design of VIA.

## Dedicated I/O Panel
This is a separate window that provides a spreadsheet like i/o interface for 
region metadata of each image. The main design philisophy is following: 
**For a single image, provide a full summary of all regions metadata in a single view window (in a spreadsheet like format)**


## Integrated I/O Panel
In this method, the i/o metadata is overlaid on the image being annotated.
The main design philisophy is following: 
**Attribute value entry and update should be possible by only using keyboard entry method**

 * [Tab] to traverse through selection of regions
 * [Enter] to enable the metadata i/o mode for the selected region.
 * [Up] and [Down] arrow keys to traverse through attributes
 * [Enter] to edit the attribute value
   * [Up] and [Down] arrow to select from a predefined list
   * Free text entry through keyboard
   * [Enter] to finish editing
 * [Up] and [Down] arrow keys to traverse through attributes
 * [Tab] to traverse through selection of regions
 * [Right] and [Left] arrow to switch to next or previous image


Abhishek Dutta  
Sep. 28, 2017
