# VIA2 User Experience (UX)

We aim to build a purely web browser based user interface that follows the 
[Google material](https://material.io/) design principles and is inspired by the 
compact and highly intutive user interface of [blender](https://www.blender.org/).

 * Theme color: https://material.io/color/#!/?view.left=0&view.right=0&primary.color=212121
```
Background color:
  Primary: #212121
    light: #484848
    dark : #000000

Text:
  on Primary : #ffffff

Grey Theme
500 #9e9e9e
 50 #fafafa
100 #f5f5f5
200 #eeeeee
300 #e0e0e0
400 #bdbdbd
500 #9e9e9e
600 #757575
700 #616161
800 #424242
900 #212121

(source: https://material.io/guidelines/style/color.html#color-color-palette)

Primary color: 500
  * color (#212121) displayed most frequently across the application's screens and components
  * to create contrast between elements, you can use lighter (#484848) or darker (#000000) tones of the primary color

Secondary color: 700 (Fallback: 300 or 900)
  * A secondary color is used to accent select parts of your UI.
  * Secondary colors are best used for:

    Buttons, floating action buttons, and button text
    Text fields, cursors, and text selection
    Progress bars
    Selection controls, buttons, and sliders
    Links
    Headlines 

Text
  White text on dark background
    Light text (#FFFFFF)    Opacity

    Primary text              100%
    Secondary text            70%
    Disabled text, hint text  50%
    Dividers                  12%

  Icons
```

## via2_ux_design.svg
```
Font      : Google Roboto (default: Sans-serif)
Theme     : Grey (from Google Material Design)
```


Abhishek Dutta  
12 July 2017

