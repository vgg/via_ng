# Git based Annotation Version Control

```
curl -X GET https://github.com/ox-vgg/via.git/info/refs?service=git-upload-pack
```
## References
 * http://gitolite.com/gitolite/fool_proof_setup/
 * [GIT HTTP Protocol](https://www.kernel.org/pub/software/scm/git/docs/technical/http-protocol.html)
 * [HTTP Basic Authentication - RFC 2617](https://en.wikipedia.org/wiki/Digest_access_authentication)
