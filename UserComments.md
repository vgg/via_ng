https://stackoverflow.com/questions/8317787/image-labelling-and-annotation-tool/43208409#43208409
  I just wanted to congratulate you for the great work on this annotation tool. I absolutely love it! Thanks! – Darien Pardinas Jul 26 at 0:17 

https://gitlab.com/vgg/via/issues/99
  Thank for very much for this wonderful tool

Email from Mark Mazumder <mazumder@csail.mit.edu> on 29 Aug. 2017
  Thanks for developing and supporting VIA, it's the best labeling tool I've used, and several people on my team have concurred after I suggested they try it! 


## VIA Usage Statistics
 * https://analytics.google.com/analytics/web/#report/content-pages/a20555581w40897376p40707104/%3F_u.date00%3D20161001%26_u.date01%3D20170917%26_r.drilldown%3Danalytics.pagePath%3A%2F~vgg%2Fsoftware%2Fvia%2F/
 * 
