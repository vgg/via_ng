
////////////////////////////////////////////////////////////////////////////////
//
// @file        _via_region.js
// @description Implementation of region shapes like rectangle, circle, etc.
// @author      Abhishek Dutta <adutta@robots.ox.ac.uk>
// @date        17 June 2017
//
////////////////////////////////////////////////////////////////////////////////

function _via_region( shape, id, data_view_space, scale_factor ) {
  // Note the following terminology:
  //   view space  :
  //     - corresponds to the x-y plane on which the scaled version of original image is shown to the user
  //     - all the region query operations like is_inside(), is_on_edge(), etc are performed in view space
  //     - all svg draw operations like get_svg() are also in view space
  //
  //   image space :
  //     - corresponds to the x-y plane which corresponds to the spatial space of the original image
  //     - region save, export, git push operations are performed in image space
  //     - to avoid any rounding issues (caused by floating scale factor),
  //        * user drawn regions in view space is first converted to image space
  //        * this region in image space is now used to initialize region in view space
  //
  //   The two spaces are related by _via_model.now.tform.scale which is computed by the method
  //     _via_ctrl.compute_view_panel_to_nowfile_tform()
  //   and applied as follows:
  //     x coordinate in image space = scale_factor * x coordinate in view space
  //
  // shape : {rect, circle, ellipse, line, polyline, polygon, point}
  // id    : unique region-id
  // d[]   : (in view space) data whose meaning depend on region shape as follows:
  //        rect     : d[x1,y1,x2,y2] or d[corner1_x, corner1_y, corner2_x, corner2_y]
  //        circle   : d[x1,y1,x2,y2] or d[center_x, center_y, circumference_x, circumference_y]
  //        ellipse  : d[x1,y1,x2,y2]
  //        line     : d[x1,y1,x2,y2]
  //        polyline : d[x1,y1,...,xn,yn]
  //        polygon  : d[x1,y1,...,xn,yn]
  //        point    : d[cx,cy,r]
  // scale_factor : for conversion from view space to image space
  //
  // Note: no svg data are stored with prefix "_". For example: _scale_factor, _x2
  this.shape  = shape;
  this.id     = id;
  this.scale_factor     = scale_factor;
  this.scale_factor_inv = 1.0 / this.scale_factor;
  this.recompute_svg    = false;
  this.attributes  = {};

  var n = data_view_space.length;
  this.dview  = new Array(n);
  this.dimg   = new Array(n);

  if ( n !== 0 ) {
    // IMPORTANT:
    // to avoid any rounding issues (caused by floating scale factor), we stick to
    // the principal that image space coordinates are the ground truth for every region.
    // Hence, we proceed as:
    //   * user drawn regions in view space is first converted to image space
    //   * this region in image space is now used to initialize region in view space
    for ( var i = 0; i < n; i++ ) {
      this.dimg[i]  = Math.round( data_view_space[i] * this.scale_factor );
      this.dview[i] = Math.round( this.dimg[i] * this.scale_factor_inv );
    }
  }

  switch( this.shape ) {
  case "rect":
    _via_region_rect.call( this );
    this.svg_attributes = ['x', 'y', 'width', 'height'];
    break;
  case "circle":
    _via_region_circle.call( this );
    this.svg_attributes = ['cx', 'cy', 'r'];
    break;
  case "ellipse":
    _via_region_ellipse.call( this );
    this.svg_attributes = ['cx', 'cy', 'rx', 'ry'];
    break;
  case "line":
    _via_region_line.call( this );
    this.svg_attributes = ['x1', 'y1', 'x2', 'y2'];
    break;
  case "polyline":
    _via_region_polyline.call( this );
    this.svg_attributes = ['points'];
    break;
  case "polygon":
    _via_region_polygon.call( this );
    this.svg_attributes = ['points'];
    break;
  case "point":
    _via_region_point.call( this );
    // point is a special circle with minimal radius required for visualization
    this.shape = 'circle';
    this.svg_attributes = ['cx', 'cy', 'r'];
    break;
  }

  this.initialize();
}


_via_region.prototype.prepare_svg_element = function() {
  var _VIA_SVG_NS = "http://www.w3.org/2000/svg";
  this.svg_element = document.createElementNS(_VIA_SVG_NS, this.shape);
  this.svg_string  = '<' + this.shape;
  this.svg_element.setAttributeNS(null, 'id', this.id);

  var n = this.svg_attributes.length;
  for ( var i = 0; i < n; i++ ) {
    this.svg_element.setAttributeNS(null, this.svg_attributes[i], this[this.svg_attributes[i]]);
    this.svg_string += ' ' + this.svg_attributes[i] + '="' + this[this.svg_attributes[i]] + '"';
  }
  this.svg_string  += '/>';
}

_via_region.prototype.get_svg_element = function() {
  if ( this.recompute_svg ) {
    this.prepare_svg_element();
    this.recompute_svg = false;
  }
  return this.svg_element;
}

_via_region.prototype.get_svg_string = function() {
  if ( this.recompute_svg ) {
    this.prepare_svg_element();
    this.recompute_svg = false;
  }
  return this.svg_string;
}

_via_region.prototype.move_dview = function( dx, dy ) {
  // IMPORTANT:
  // to avoid any rounding issues (caused by floating scale factor), we stick to
  // the principal that image space coordinates are the ground truth for every region.
  // Hence, we proceed as:
  //   * user drawn regions in view space is first converted to image space
  //   * this region in image space is now used to initialize region in view space

  var n = this.dview.length;
  for ( var i = 0; i < n; i += 2 ) {
    var view_x = this.dview[i] + dx;
    var view_y = this.dview[i+1] + dy;
    this.dimg[i]   = Math.round( view_x * this.scale_factor );
    this.dimg[i+1] = Math.round( view_y * this.scale_factor );

    this.dview[i]   = Math.round( this.dimg[i] * this.scale_factor_inv );
    this.dview[i+1] = Math.round( this.dimg[i+1] * this.scale_factor_inv );
  }
}
_via_region.prototype.set_vertex_dview = function( vertex, x, y ) {
  // IMPORTANT:
  // to avoid any rounding issues (caused by floating scale factor), we stick to
  // the principal that image space coordinates are the ground truth for every region.
  // Hence, we proceed as:
  //   * user drawn regions in view space is first converted to image space
  //   * this region in image space is now used to initialize region in view space

  var i = 2 * vertex;
  this.dimg[i]   = Math.round( x * this.scale_factor );
  this.dimg[i+1] = Math.round( y * this.scale_factor );

  this.dview[i]   = Math.round( this.dimg[i]   * this.scale_factor_inv );
  this.dview[i+1] = Math.round( this.dimg[i+1] * this.scale_factor_inv );
}

_via_region.prototype.set_vertex_x_dview = function (vertex, x) {
  var i = 2 * vertex;
  this.dimg[i]   = Math.round( x * this.scale_factor );
  this.dview[i]  = Math.round( this.dimg[i]   * this.scale_factor_inv );
}

_via_region.prototype.set_vertex_y_dview = function (vertex, y) {
  var i = 2 * vertex;
  this.dimg[i+1]   = Math.round( y * this.scale_factor );
  this.dview[i+1]  = Math.round( this.dimg[i+1]   * this.scale_factor_inv );
}

///
/// Region shape : rectangle
///
function _via_region_rect() {
  this.is_inside  = _via_region_rect.prototype.is_inside;
  this.is_on_edge = _via_region_rect.prototype.is_on_edge;
  this.move  = _via_region_rect.prototype.move;
  this.resize  = _via_region_rect.prototype.resize;
  this.initialize = _via_region_rect.prototype.initialize;
  this.dist_to_nearest_edge = _via_region_rect.prototype.dist_to_nearest_edge;
}

_via_region_rect.prototype.initialize = function() {
  // ensure that this.(x,y) corresponds to top-left corner of rectangle
  // Note: this.(x2,y2) is defined for convenience in calculations
  if ( this.dview[0] < this.dview[2] ) {
    this.x  = this.dview[0];
    this.x2 = this.dview[2];
  } else {
    this.x  = this.dview[2];
    this.x2 = this.dview[0];
  }
  if ( this.dview[1] < this.dview[3] ) {
    this.y  = this.dview[1];
    this.y2 = this.dview[3];
  } else {
    this.y  = this.dview[3];
    this.y2 = this.dview[1];
  }
  this.width  = this.x2 - this.x;
  this.height = this.y2 - this.y;
  this.recompute_svg = true;
}

_via_region_rect.prototype.is_inside = function( px, py ) {
  if ( px > this.x && px < this.x2 ) {
    if ( py > this.y && py < this.y2 ) {
      return true;
    }
  }
  return false;
}

_via_region_rect.prototype.is_on_edge = function( px, py, tolerance ) {
  // -1 indicates that (px,py) is neither on edge nor on corners
  // 1 : corner - top left
  // 2 : edge   - top
  // 3 : corner - top right
  // 4 : edge   - right
  // 5 : corner - bottom right
  // 6 : edge   - bottom
  // 7 : corner - bottom left
  // 8 : edge   - left
  var dx0 = Math.abs( this.x  - px );
  var dy0 = Math.abs( this.y  - py );
  var dx2 = Math.abs( this.x2 - px );
  var dy2 = Math.abs( this.y2 - py );

  if ( dx0 < tolerance && dy2 < tolerance ) {
    return 7;
  }

  if ( dx2 < tolerance && dy0 < tolerance ) {
    return 3;
  }

  if ( dx0 < tolerance ) {
    if ( dy0 < tolerance ) {
      return 1;
    } else {
      if ( py > this.y && py < this.y2 ) {
        return 8;
      }
    }
  } else {
    if ( dy0 < tolerance ) {
      if ( px > this.x && px < this.x2 ) {
        return 2;
      }
    }
  }

  if ( dx2 < tolerance ) {
    if ( dy2 < tolerance ) {
      return 5;
    } else {
      if ( py < this.y2 && py > this.y ) {
        return 4;
      }
    }
  } else {
    if ( dy2 < tolerance ) {
      if ( px > this.x && px < this.x2 ) {
        return 6;
      }
    }
  }

  return -1; // not on edge
}

_via_region_rect.prototype.dist_to_nearest_edge = function( px, py ) {
  // given the coordinates of two opposite corners of a rectangle
  // (dview[0], dview[1]) and (dview[2], dview[3])
  // plot the following points and you will see why this works
  var d = [Math.abs(py - this.dview[1]),
           Math.abs(py - this.dview[3]),
           Math.abs(px - this.dview[0]),
           Math.abs(px - this.dview[2])];
  return Math.min.apply(null, d);
}

_via_region_rect.prototype.move = function(dx, dy) {
  this.move_dview(dx, dy);
  this.initialize();
}

_via_region_rect.prototype.resize = function(vertex, x, y) {
  // see _via_region_rect() for definition of constants
  switch (vertex) {
  case 1:
    this.set_vertex_dview(0, x, y);
    break;
  case 2:
    this.set_vertex_y_dview(0, y);
    break;
  case 3:
    this.set_vertex_y_dview(0, y);
    this.set_vertex_x_dview(1, x);
    break;
  case 4:
    this.set_vertex_x_dview(1, x);
    break;
  case 5:
    this.set_vertex_dview(1, x, y);
    break;
  case 6:
    this.set_vertex_y_dview(1, y);
    break;
  case 7:
    this.set_vertex_x_dview(0, x);
    this.set_vertex_y_dview(1, y);
    break;
  case 8:
    this.set_vertex_x_dview(0, x);
    break;
  }
  this.initialize();
}

///
/// Region shape : circle
///
function _via_region_circle() {
  this.is_inside  = _via_region_circle.prototype.is_inside;
  this.is_on_edge = _via_region_circle.prototype.is_on_edge;
  this.move       = _via_region_circle.prototype.move;
  this.resize     = _via_region_circle.prototype.resize;
  this.initialize = _via_region_circle.prototype.initialize;
  this.dist_to_nearest_edge = _via_region_circle.prototype.dist_to_nearest_edge;
}

_via_region_circle.prototype.initialize = function() {
  this.cx = this.dview[0];
  this.cy = this.dview[1];
  var dx = this.dview[2] - this.dview[0];
  var dy = this.dview[3] - this.dview[1];
  this.r  = Math.round( Math.sqrt(dx * dx + dy * dy) );
  this.r2 = this.r * this.r;
  this.recompute_svg = true;
}

_via_region_circle.prototype.is_inside = function( px, py ) {
  var dx = px - this.cx;
  var dy = py - this.cy;
  if ( ( dx * dx + dy * dy ) < this.r2 ) {
    return true;
  } else {
    return false;
  }
}

_via_region_circle.prototype.is_on_edge = function( px, py, tolerance ) {
  // -1 indicates that (px,py) is neither on edge nor on corners
  // 1 : corner - top left (-00 to -70 degrees)
  // 2 : edge   - top (-0 to -00 degrees)
  // 3 : corner - top right (-10 to -80 degrees)
  // 4 : edge   - right (-10 to +10 degrees)
  // 5 : corner - bottom right (10 to 80 degrees)
  // 6 : edge   - bottom (80 to 100 degrees)
  // 7 : corner - bottom left (100 to 170 degrees)
  // 8 : edge   - left (>170 and < -170 degrees)
  var tlist = [ 0.174, 1.396, 1.745, 2.967 ];
  var dx = px - this.cx;
  var dy = py - this.cy;
  var dxdy2 = dx*dx + dy*dy;

  var ra = this.r - tolerance;
  var rb = this.r + tolerance;

  if ( dxdy2 >= (ra*ra) && dxdy2 <= (rb*rb) ) {
    var t = Math.atan2( py - this.cy, px - this.cx );
    if ( t >=  tlist[0] && t <=  tlist[1] ) {
      return 5;
    }
    if ( t >= -tlist[1] && t <= -tlist[0] ) {
      return 3;
    }
    if ( t >=  tlist[2] && t <=  tlist[3] ) {
      return 7;
    }
    if ( t >= -tlist[3] && t <= -tlist[2] ) {
      return 1;
    }

    if ( t >= -tlist[0] && t <=  tlist[0] ) {
      return 4;
    }
    if ( t >=  tlist[1] && t <=  tlist[2] ) {
      return 6;
    }
    if ( t >=  tlist[3] || t <= -tlist[3] ) {
      return 8;
    }
    if ( t >= -tlist[2] && t <= -tlist[1] ) {
      return 2;
    }
  }
  return -1;
}

_via_region_circle.prototype.dist_to_nearest_edge = function( px, py ) {
  // distance to edge = radius - distance_to_point
  var dx = px - this.cx;
  var dy = py - this.cy;
  var dp = Math.sqrt(dx * dx + dy * dy);
  return this.r - dp;
}

_via_region_circle.prototype.move = function( dx, dy ) {
  this.move_dview( dx, dy );
  this.initialize();
}

_via_region_circle.prototype.resize = function( vertex, x_new, y_new ) {
  // whatever is the vertex, we always resize the circle boundary
  this.set_vertex_dview(1, x_new, y_new);
  this.initialize();
}

///
/// Region shape : ellipse
///
function _via_region_ellipse() {
  this.is_inside  = _via_region_ellipse.prototype.is_inside;
  this.is_on_edge = _via_region_ellipse.prototype.is_on_edge;
  this.move  = _via_region_ellipse.prototype.move;
  this.resize  = _via_region_ellipse.prototype.resize;
  this.initialize = _via_region_ellipse.prototype.initialize;
  this.dist_to_nearest_edge = _via_region_ellipse.prototype.dist_to_nearest_edge;
}

_via_region_ellipse.prototype.initialize = function() {
  this.cx = this.dview[0];
  this.cy = this.dview[1];
  this.rx = Math.abs(this.dview[2] - this.dview[0]);
  this.ry = Math.abs(this.dview[3] - this.dview[1]);

  this.inv_rx2 = 1 / (this.rx * this.rx);
  this.inv_ry2 = 1 / (this.ry * this.ry);

  this.recompute_svg = true;
}

_via_region_ellipse.prototype.is_inside = function(px, py) {
  var dx = this.cx - px;
  var dy = this.cy - py;

  if ( ( (dx * dx * this.inv_rx2) + (dy * dy * this.inv_ry2) ) < 1 ) {
    return true;
  } else {
    return false;
  }
}

_via_region_ellipse.prototype.is_on_edge = function( px, py, tolerance ) {
  // -1 indicates that (px,py) is neither on edge nor on corners
  // 1 : corner - top left (-00 to -70 degrees)
  // 2 : edge   - top (-0 to -00 degrees)
  // 3 : corner - top right (-10 to -80 degrees)
  // 4 : edge   - right (-10 to +10 degrees)
  // 5 : corner - bottom right (10 to 80 degrees)
  // 6 : edge   - bottom (80 to 100 degrees)
  // 7 : corner - bottom left (100 to 170 degrees)
  // 8 : edge   - left (>170 and < -170 degrees)
  var tlist = [ 0.174, 1.396, 1.745, 2.967 ];

  var d1rx = this.rx - tolerance;
  var d1ry = this.ry - tolerance;
  var d2rx = this.rx + tolerance;
  var d2ry = this.ry + tolerance;

  var inv_d1rx2 = 1 / (d1rx * d1rx);
  var inv_d1ry2 = 1 / (d1ry * d1ry);
  var inv_d2rx2 = 1 / (d2rx * d2rx);
  var inv_d2ry2 = 1 / (d2ry * d2ry);

  var dx = px - this.cx;
  var dy = py - this.cy;
  var dx2 = dx * dx;
  var dy2 = dy * dy;

  if ( (dx2 * inv_d1rx2) + (dy2 * inv_d1ry2) >= 1 &&
       (dx2 * inv_d2rx2) + (dy2 * inv_d2ry2) <= 1
     ) {
    var t = Math.atan2(dy, dx);
    if ( t >=  tlist[0] && t <=  tlist[1] ) {
      return 5;
    }
    if ( t >= -tlist[1] && t <= -tlist[0] ) {
      return 3;
    }
    if ( t >=  tlist[2] && t <=  tlist[3] ) {
      return 7;
    }
    if ( t >= -tlist[3] && t <= -tlist[2] ) {
      return 1;
    }

    if ( t >= -tlist[0] && t <=  tlist[0] ) {
      return 4;
    }
    if ( t >=  tlist[1] && t <=  tlist[2] ) {
      return 6;
    }
    if ( t >=  tlist[3] || t <= -tlist[3] ) {
      return 8;
    }
    if ( t >= -tlist[2] && t <= -tlist[1] ) {
      return 2;
    }
  }
  return -1;
}

_via_region_ellipse.prototype.dist_to_nearest_edge = function( px, py ) {
  // stretch the line connecting ellipse center and (px,py) towards the ellipse edge
  // length of this line is given by the polar form of ellipse relative to center
  // for more details: see https://en.wikipedia.org/wiki/Ellipse#Polar_forms
  var dx = px - this.cx;
  var dy = py - this.cy;
  var h = Math.sqrt( dx*dx + dy*dy ); // hypotenuse
  var hinv = 1 / h;
  var p = py - this.cy; // perpendicular
  var b = px - this.cx; // base
  var rx_sint = this.rx * p * hinv;
  var ry_cost = this.ry * b * hinv;
  var sqterm = Math.sqrt( ry_cost*ry_cost + rx_sint*rx_sint );
  var r_theta = (this.rx * this.ry ) / sqterm;
  return Math.abs(r_theta - h);
}

_via_region_ellipse.prototype.move = function(dx, dy) {
  this.move_dview(dx, dy);
  this.initialize();
}

_via_region_ellipse.prototype.resize = function(vertex, x, y) {
  switch (vertex) {
  case 2:
  case 6:
    this.set_vertex_y_dview(1, y);
    break;
  case 4:
  case 8:
    this.set_vertex_x_dview(1, x);
    break;
  }
  this.initialize();
}

///
/// Region shape : line
///
function _via_region_line() {
  this.is_inside  = _via_region_line.prototype.is_inside;
  this.is_on_edge = _via_region_line.prototype.is_on_edge;
  this.move  = _via_region_line.prototype.move;
  this.resize  = _via_region_line.prototype.resize;
  this.initialize = _via_region_line.prototype.initialize;
  this.dist_to_nearest_edge = _via_region_line.prototype.dist_to_nearest_edge;
}

_via_region_line.prototype.initialize = function() {
  this.x1 = this.dview[0];
  this.y1 = this.dview[1];
  this.x2 = this.dview[2];
  this.y2 = this.dview[3];
  this.dx = this.x1 - this.x2;
  this.dy = this.y1 - this.y2;
  this.mconst = (this.x1 * this.y2) - (this.x2 * this.y1);

  this.recompute_svg = true;
}

_via_region_line.prototype.is_inside = function( px, py ) {
  // compute the area of a triangle made up of the following three vertices
  // (x1,y1) (x2,y2) and (px,py)
  //        | px py 1 |
  // Area = | x1 y1 1 | = px(y1 - y2) - py(x1 - x2) + (x1*y2 - x2*y1)
  //        | x2 y2 1 |
  var area = Math.abs( px*this.dy - py*this.dx + this.mconst );
  var area_tolerance = Math.abs( 5 * (this.dy + this.dx) ); // area diff. when (x1,y1) moved by 5 pixel

  if ( area <= area_tolerance ) {
    // check if (px,py) lies between (x0,y0) and (x1,y1)
    if ( (px > this.x1 && px < this.x2) || (px < this.x1 && px > this.x2) ) {
      if ( (py > this.y1 && py < this.y2) || (py < this.y1 && py > this.y2) ) {
        return true;
      }
    }
  } else {
    return false;
  }
}

_via_region_line.prototype.dist_to_nearest_edge = function( px, py ) {
  var dx = this.x2 - this.x1;
  var dy = this.y2 - this.y1;
  // see https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Line_defined_by_two_points
  var x2y1 = this.x2 * this.y1;
  var y2x1 = this.y2 * this.x1;
  var denominator = Math.sqrt( dx*dx + dy*dy );
  var numerator   = Math.abs( dy*px - dx*py + x2y1 - y2x1 );
  return ( numerator / denominator );
}

_via_region_line.prototype.is_on_edge = function( px, py, tolerance ) {
  // -1 indicates that (px,py) is not on edge
  // N  indicates near vertex N
  var tolerance2 = tolerance * tolerance;
  var dx1 = px - this.x1;
  var dy1 = py - this.y1;
  var dx2 = px - this.x2;
  var dy2 = py - this.y2;

  if (dx1*dx1 + dy1*dy1 <= tolerance2) {
    return 0;
  }

  if (dx2*dx2 + dy2*dy2 <= tolerance2) {
    return 1;
  }

  return -1;
}

_via_region_line.prototype.move = function( dx, dy ) {
  this.move_dview(dx, dy);
  this.initialize();
}

_via_region_line.prototype.resize = function(vertex, x, y) {
  this.set_vertex_dview(vertex, x, y);
  this.initialize();
}

///
/// Region shape : polyline
///
function _via_region_polyline() {
  this.is_inside  = _via_region_polyline.prototype.is_inside;
  this.is_on_edge = _via_region_polyline.prototype.is_on_edge;
  this.move  = _via_region_polyline.prototype.move;
  this.resize  = _via_region_polyline.prototype.resize;
  this.initialize = _via_region_polyline.prototype.initialize;
  this.dist_to_nearest_edge = _via_region_polyline.prototype.dist_to_nearest_edge;
}

_via_region_polyline.prototype.initialize = function() {
  var n = this.dview.length;
  var points = new Array(n/2);
  var points_index = 0;
  for ( var i = 0; i < n; i += 2 ) {
    points[points_index] = ( this.dview[i] + ' ' + this.dview[i+1] );
    points_index++;
  }
  this.points = points.join(',');
  this.recompute_svg = true;
}

_via_region_polyline.prototype.is_inside = function( px, py ) {
  // @todo: optimize
  var tolerance = 10;
  var n = this.dview.length;

  // check if (px,py) is near any vertex
  for ( var i = 0; i < n; i += 2 ) {
    var dx = this.dview[i] - px;
    var dy = this.dview[i+1] - py;

    if ( (dx*dx + dy*dy) <= tolerance ) {
      return true;
    }
  }

  var n = this.dview.length - 2;
  for ( var i = 0; i < n; i += 2 ) {
    var x0 = this.dview[i];
    var y0 = this.dview[i+1];
    var x1 = this.dview[i+2];
    var y1 = this.dview[i+3];

    var dx = x0 - x1;
    var dy = y0 - y1;
    var mconst = x0*y1 - x1*y0;
    var area = Math.abs( px*dy - py*dx + mconst );
    var area_tolerance = Math.abs( 5*(dx+dy) );
    if ( area <= area_tolerance ) {
      // check if (px,py) lies between (x0,y0) and (x1,y1)
      if ( (px > x0 && px < x1) || (px < x0 && px > x1) ) {
        if ( (py > y0 && py < y1) || (py < y0 && py > y1) ) {
          return true;
        }
      }
    }
  }
  return false;
}

_via_region_polyline.prototype.dist_to_nearest_edge = function( px, py ) {
  var n = this.dview.length;
  var vertex = 0;
  var distances = [];
  for ( var i = 0; i < n; i += 2 ) {
    var dx = this.dview[i+2] - this.dview[i];
    var dy = this.dview[i+3] - this.dview[i+1];
    // see https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Line_defined_by_two_points
    var x2y1 = this.dview[i+2] * this.dview[1];
    var y2x1 = this.dview[i+3] * this.dview[0];
    var denominator = Math.sqrt( dx*dx + dy*dy );
    var numerator   = Math.abs( dy*px - dx*py + x1y1 - y2x1 );
    distances.push( numerator / denominator );
  }
  return Math.min.apply(null, distances);
}

_via_region_polyline.prototype.is_on_edge = function( px, py, tolerance ) {
  // -1 indicates that (px,py) is not on edge
  // return vertex index when (px,py) is within tolerance of a vertex (not edge)
  var n = this.dview.length;
  var vertex = 0;
  for ( var i = 0; i < n; i += 2 ) {
    var dx = this.dview[i] - px;
    var dy = this.dview[i+1] - py;

    if ( (dx*dx + dy*dy) <= tolerance ) {
      return vertex;
    }
    vertex++;
  }
  return -1;
}

_via_region_polyline.prototype.move = function( dx, dy ) {
  this.move_dview(dx, dy);
  this.initialize();
}

_via_region_polyline.prototype.resize = function( vertex, x, y ) {
  this.set_vertex_dview(vertex, x, y);
  this.initialize();
}

///
/// Region shape : polygon
///
function _via_region_polygon() {
  this.is_inside  = _via_region_polygon.prototype.is_inside;
  this.is_on_edge = _via_region_polygon.prototype.is_on_edge;
  this.move  = _via_region_polygon.prototype.move;
  this.resize  = _via_region_polygon.prototype.resize;
  this.initialize = _via_region_polygon.prototype.initialize;
  this.dist_to_nearest_edge = _via_region_polygon.prototype.dist_to_nearest_edge;
}

_via_region_polygon.prototype.initialize = function() {
  var n = this.dview.length;
  var points = new Array(n/2);
  var points_index = 0;
  for ( var i = 0; i < n; i += 2 ) {
    points[points_index] = ( this.dview[i] + ' ' + this.dview[i+1] );
    points_index++;
  }
  this.points = points.join(',');
  this.recompute_svg = true;
}

_via_region_polygon.prototype.is_inside = function( px, py ) {
  // ref: http://geomalgorithms.com/a03-_inclusion.html
  var n = this.dview.length;

  var wn = 0;    // the  winding number counter
  // loop through all edges of the polygon
  for ( var i = 0; i < n; i += 2 ) {   // edge from V[i] to  V[i+1]
    var x0 = this.dview[i];
    var y0 = this.dview[i+1];
    var x1 = this.dview[i+2];
    var y1 = this.dview[i+3];

    // area of triangle is 0 if points are collinear
    var is_left_value =  ((x1 - x0) * (py - y0)) - ((y1 - y0) * (px - x0));

    if ( y0 <= py ) {
      if ( y1  > py && is_left_value > 0) {
        ++wn;
      }
    }
    else {
      if ( y1  <= py && is_left_value < 0) {
        --wn;
      }
    }
  }
  if ( wn === 0 ) {
    return false;
  }
  else {
    return true;
  }
}

_via_region_polygon.prototype.dist_to_nearest_edge = function( px, py ) {
  // create a copy of vertices and add first vertex to close path
  var n = this.dview.length;
  var points = this.dview.slice(0);
  points.push(points[0]);
  points.push(points[1]);

  var vertex = 0;
  var distances = [];

  for ( var i = 0; i < n; i += 2 ) {
    var dx = points[i+2] - points[i];
    var dy = points[i+3] - points[i+1];

    // see https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Line_defined_by_two_points
    var x2y1 = points[i+2] * points[i+1];
    var y2x1 = points[i+3] * points[i];
    var denominator = Math.sqrt( dx*dx + dy*dy );
    var numerator   = Math.abs( dy*px - dx*py + x2y1 - y2x1 );

    distances.push( numerator / denominator );
  }
  return Math.min.apply(null, distances);
}

_via_region_polygon.prototype.is_on_edge = function( px, py, tolerance ) {
  // -1 indicates that (px,py) is not on edge
  // -1 indicates that (px,py) is not on edge
  // return vertex index when (px,py) is within tolerance of a vertex (not edge)
  var n = this.dview.length;
  var vertex = 0;
  for ( var i = 0; i < n; i += 2 ) {
    var dx = this.dview[i] - px;
    var dy = this.dview[i+1] - py;

    if ( (dx*dx + dy*dy) <= tolerance ) {
      return vertex;
    }
    vertex++;
  }
  return -1;
}

_via_region_polygon.prototype.move = function( dx, dy ) {
  this.move_dview(dx, dy);
  this.initialize();
}

_via_region_polygon.prototype.resize = function( vertex, x, y ) {
  this.set_vertex_dview(vertex, x, y);
  this.initialize();
}

///
/// Region shape : point
///
function _via_region_point() {
  this.is_inside  = _via_region_point.prototype.is_inside;
  this.is_on_edge = _via_region_point.prototype.is_on_edge;
  this.move  = _via_region_point.prototype.move;
  this.resize  = _via_region_point.prototype.resize
  this.initialize  = _via_region_point.prototype.initialize;
  this.dist_to_nearest_edge = _via_region_point.prototype.dist_to_nearest_edge;
}

_via_region_point.prototype.initialize = function() {
  this.cx = this.dview[0];
  this.cy = this.dview[1];
  this.r  = 3;
  this.r2 = this.r * this.r;
  this.recompute_svg = true;
}

_via_region_point.prototype.is_inside = function( px, py ) {
  var dx = px - this.cx;
  var dy = py - this.cy;
  if ( ( dx * dx + dy * dy ) < this.r2 ) {
    return true;
  } else {
    return false;
  }
}

_via_region_point.prototype.dist_to_nearest_edge = function( px, py ) {
  var dx = px - this.cx;
  var dy = py - this.cy;
  return Math.sqrt( dx * dx + dy * dy );
}

_via_region_point.prototype.is_on_edge = function( px, py, tolerance ) {
  // -1 indicates that (px,py) is not on edge
  return -1;
}

_via_region_point.prototype.move = function( dx, dy ) {
  this.move_dview(dx, dy);
  this.initialize();
}

_via_region_point.prototype.resize = function( vertex, dx, dy ) {
  return;
}
